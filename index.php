<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>NewsCrawl | Cari Berita Instan</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <script src="assets/js/ie-emulation-modes-warning.js"></script>
  <link rel="shortcut icon" href="assets/img/favicon-newscrawl.png" > 
  <script src="assets/js/jquery-2.1.4.min.js"> </script>
  <script src="assets/js/bootstrap.min.js"></script>
  <style>
</style>
</head>
<body>


<nav class="navbar navbar-default" style="background-color: white">
  <div class="container-fluid">    
    <ul class="nav navbar-nav navbar-right">
          <li><a href="bankberita.php" style="color:black"><i class="fa fa-file"></i> Data Bank Berita</a></li>
    </ul>
    </div>
</nav>

<br><br><br><br><br><br><br><br><br>
<div class="container">
      <center><img src="assets/img/logo-newscrawl.png" height="40%" width="40%"></center>
      <br>
      <div class="form-group">
  <div class="row">
  <div class="col-lg-3"></div>
  <div class="col-lg-6">
  <form action="hasilcrawler.php" method="POST">
  <div class="input-group">
    <input type="text" name="url" placeholder="Masukkan URL..." class="form-control">
    <span class="input-group-btn">
      <button class="btn btn-primary" type="submit" ><i class="fa fa-search"></i></button>
    </span>
  </div>
  </form>

  </div>
  <div class="col-lg-3"></div>
  </div>
</div>
  <div class="container">
    <div class="page-header"> </div>
      <footer class="footer">
        <div class="container">
          <center><p class="text-muted">NewsCrawl © 2019<br>Jabir Al Hayyan</p></center>
        </div>
      </footer>
   </div>
</body>
</html>