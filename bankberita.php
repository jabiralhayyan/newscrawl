<?php
  include('config/database.php');
  $query = mysqli_query($conn, "SELECT DISTINCT * FROM nc_berita ORDER BY idberita ASC");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Crawl | Bank Berita</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="assets/css/datatables/dataTables.bootstrap.min.css">
  <script src="assets/js/ie-emulation-modes-warning.js"></script>
  <link rel="shortcut icon" href="assets/img/favicon-newscrawl.png" > 
  <script src="assets/js/jquery-2.1.4.min.js"> </script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" charset="utf8" src="assets/js/datatables/jquery.dataTables.js"></script>
  <style>
</style>
</head>
<body>

<nav class="navbar navbar-default" style="background-color: #E6E6E6">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="index.php"><img src="assets/img/logo-newscrawl.png" height="20%" width="20%"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->

      <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="bankberita.php"><i class="fa fa-file"></i> Data Bank Berita</a></li>
      </ul>
  </div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  
  <div class="container">
    
    <ul class="nav nav-tabs">
      <li role="presentation" class="active"><a href="#"><i class="fa fa-file"></i> Data Bank Berita</a></li>
    </ul>

    <div class="page-header">
    <h4>Data Bank Berita</h4>
    </div>

    <div class="pull-right">
    <!-- <a href="export.php" type="button" target="_blank" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Export PDF</a> -->
    </div>
    <br><br>

    <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Judul</th>
                <th>Deskripsi</th>
                <th>Link</th>
                <th>Kategori</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
            <?php
                if(mysqli_num_rows($query) > 0 )
                {
                  while($row = mysqli_fetch_array($query))
                  {
                    $idberita = $row['idberita'];
            ?>   
                      <tr>
                        <td><?php echo $row['idberita']; ?></td>
                        <td><?php echo SUBSTR($row['judul'],0, 20).' ....'; ?></td>
                        <td><?php echo SUBSTR($row['deskripsi'], 0, 40).' ....'; ?></td>
                        <td><a href="<?php echo $row['link']; ?>" target="_blank"><?php echo $row['link']; ?></a></td>
                        <td><?php if($row['kategori'] == '1') echo 'No Clickbait'; ?></td>
                        <td>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#lihatberita<?php echo $idberita; ?>"><i class="fa fa-search"></i> Lihat</button>

                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusberita<?php echo $idberita; ?>"><i class="fa fa-trash"></i> Hapus</button>

                            <!-- Modal Hapus Berita -->
                      <div class="modal fade" id="hapusberita<?php echo $idberita; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                            </div>
                              <form role="form" method="post" action="hapusberita.php?idberita=<?php echo $row['idberita']; ?>">
                              <div class="modal-body">       
                                <div class="box-body">
                                  <p style="font-size: 22px"><b>Apakah anda yakin ingin menghapus berita dengan dibawah ini ?</b></p>
                                  <p><b><?php echo $row['judul']; ?></b></p>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                                <button type="submit" class="btn btn-danger" >Ya</button>
                                </div>
                                </form>
                            </div>
                          </div>
                        </div>


                        <!-- Modal Lihat Berita -->
                        <div class="modal fade" id="lihatberita<?php echo $idberita; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel"><?php echo $row['judul']; ?></h4>
                              </div>
                              <div class="modal-body">       
                                <div class="box-body">
                                  <?php echo $row['berita']; ?>
                                </div>
                                <!-- /.box-body -->
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                              </div>
                            </div>
                          </div>
                        </div>


                        </td>
                       </tr>

                  <?php  } } ?>
                 
        </tbody>
  </table>
    
 

  </div>

  <div class="container">
    <div class="page-header"> </div>
      <footer class="footer">
        <div class="container">
          <center><p class="text-muted">Crawler Data Bank Berita © 2019<br>Jabir Al Hayyan</p></center>
        </div>
      </footer>
   </div>

<script>
   $(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>

</body>
</html>