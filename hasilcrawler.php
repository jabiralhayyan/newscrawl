<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>NewsCrawl | Cari Berita Instan</title>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  <script src="assets/js/ie-emulation-modes-warning.js"></script>
  <link rel="shortcut icon" href="assets/img/favicon-newscrawl.png" > 
  <script src="assets/js/jquery-2.1.4.min.js"> </script>
  <script src="assets/js/bootstrap.min.js"></script>
  <style>
</style>
</head>
<body>

<nav class="navbar navbar-default" style="background-color: #E6E6E6">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="index.php"><img src="assets/img/favicon-newscrawl.png" height="60px" width="60px"></a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->

      <form action="hasilcrawler.php" method="post">
      <div class="col-xs-3">
      <div class="input-group"> 
        <span class="input-group-btn">
        <input type="search" name="url" placeholder="Masukkan URL..." class="form-control" value="">
          <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
        </span>
      </div>
      </div>
      </form>

      <ul class="nav navbar-nav navbar-right">
          <li><a href="bankberita.php" style="color:black"><i class="fa fa-file"></i> Data Bank Berita</a></li>
      </ul>

  </div>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

 <div class="row">
 <div class="col-md-1"></div>
 <div class="col-md-9">
 
 <?php
    $crawlURL=$_POST['url'];
    function isDomainAvailible($domain){
     if(!filter_var($domain, FILTER_VALIDATE_URL)){
      return false;
     }
     $curlInit = curl_init($domain);
     curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
     curl_setopt($curlInit,CURLOPT_HEADER,true);
     curl_setopt($curlInit,CURLOPT_NOBODY,true);
     curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
     $response = curl_exec($curlInit);
     curl_close($curlInit);
     if ($response) return true;
     return false;
    }
    if(!isDomainAvailible($crawlURL)){
     echo "<h3>Silahkan masukkan URL yang valid.</h3> URL anda tidak bisa diakses.";
    }else{
     flush();
     echo "<p>Crawling :".$crawlURL.'</p>';
     include("crawler.php");
    }

   ?>

   
   </div>
   </div>

  <div class="container">
    <div class="page-header"> </div>
      <footer class="footer">
        <div class="container">
          <center><p class="text-muted">NewsCrawl © 2017<br>Jabir Al Hayyan</p></center>
        </div>
      </footer>
   </div>
</body>
</html>