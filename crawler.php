<?
include("libraries/simple_html_dom.php");

// Page Successfully Got
$html = file_get_html($crawlURL);
if(is_object($html)){
	//mengambil title
	$t = $html->find("title", 0);
	if($t){
		$title = $t->plaintext;
		//pisahtitle berupa array dari title web yg sudah dipisah
		$pisahtitle = explode(" ", $title);
		//silahkan bisa ditambah algoritma untuk Bayes (Membandingkan antara Judul Berita dan Isinya)		
	}
	//mengambil deskripsi meta content
	if (getMetaDescription($html)) {
		$metadescription = getMetaDescription($html);
	}
	//mengambil content web
	if(getContent($html) != false) {
		$contentweb = getContent($html);
	}

	//var_dump($contentweb);
	hasilCrawling($crawlURL, $title, $metadescription, $contentweb);

	$html->clear(); 
	unset($html);
}

	function hasilCrawling($crawlURL, $title, $metadescription, $contentweb)
	{
		echo'
			<div class="col-md-12">
				<table class="table table-hover">
					<tbody>
						<tr>
					<td style="width:800px">
						<a href="'.$crawlURL.'" style="text-decoration:none;font-size: 24px;color:#1A0DAB" target="_blank">'.($title ? $title : "").'</a><br>
						<a style="text-decoration:none;color:#006621">'.$crawlURL.'/</a>
						<p>'.($metadescription ? $metadescription : "").'</p>
					</td>
					<td>
						<center>
							<br>
							<form method="POST" action="simpanberita.php" target="_blank">

							<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#lihatberita"><i class="fa fa-search"></i> Content Web</button>
							
							<input type="hidden" name="link" value="'.$crawlURL.'">
							<textarea name="berita" style="display:none;">'.$contentweb.'</textarea>
						    <input type="hidden" name="judul" value="'.$title.'">
						    <input type="hidden" name="deskripsi" value="'.$metadescription.'">
						    <input type="hidden" name="kategori" value="1">

							    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-download"></i> Simpan Berita</button>
							</form>

						</center>

						<!-- Modal Berita -->
						<div class="modal fade" id="lihatberita" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">'.$title.'</h4>
									</div>
									<div class="modal-body">       
										<div class="box-body">'.$contentweb.'</div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
					</tbody>
				</table>
			</div>
		';
	}

	function getMetaDescription($html)
	{
			//mengambil meta-content 
			foreach($html->find('meta[name=description]') as $meta){
				$metadescription = $meta->content;
			}
			return $metadescription;
	}

	function getContent($html)
	{
		$notifikasi = 'Maaf, Tidak Ada Konten Berita !';
		$tagpost1 = $html->find('div[class=read__content]'); //tagpost kompas.com
		$tagpost2 = $html->find('div[class=news-post-content clearfix]'); //tagpost wartaekonomi.co.id
		$tagpost3 = $html->find('div[class=content-list-component text]'); //tagpost indonews.id

		foreach($tagpost1 as $post) {
			$text1 = $post->plaintext;
		}
		foreach($tagpost2 as $post) {
			$text2 = $post->plaintext;
		}
		foreach($tagpost3 as $post) {
			$text3 = $post->plaintext;
		}


		//Condition konten kompas.com
		if($text1!= NULL) {
			$text1 = str_replace("<p>", "", $text1); //menghilangkan tag <p>
			$text1 = str_replace("</p>", "", $text1); //menghilangkan tag </p>
			$text1 = str_replace("Baca juga:", "", $text1); //menghilangkan tulisan baca juga
			$text1 = trim(preg_replace('/\s+/', ' ', $text1));
			return $text1;
		}
		//Condition konten wartaekonomi.co.id
		if($text2!= NULL) {
			$text2 = str_replace("Baca Juga:", "", $text2); //menghilangkan tulisan baca juga
			$text2 = trim(preg_replace('/\s+/', ' ', $text2));
			return $text2;
		}
		//Condition konten indonews.id
		if($text3!= NULL) {
			$text3 = str_replace("Baca juga : ", "", $text3); //menghilangkan tulisan baca juga
			$text3 = trim(preg_replace('/\s+/', ' ', $text3));
			return $text3;
		}
		else return $notifikasi;

	}